# HTML to Markdown conversion
The goal of this project is to convert HTML to Markdown. At this moment
no specific flavor has been chosen and the best architecture is being
discovered.

A planned extension is creating a version specific supporting maximum
line lengths, which can be used for both online and e-mails to improve
readability of the resulting Markdown itself.

# Design goals
- Streaming design enabling large and small HTML pieces
- Tokenization and conversion of tokens to enable easier processing
- Strategy design enabling different kinds processing (e.g. Markdown
  flavors and maximum line length)
- Semi high performance by leveraging new constructs such as `Span<T>`
  and `Memory<T>` when working with text conversion and manipulation
- Not forgiving when it comes to HTML: garbage in = garbage out

# Research notes
- `Pipe` with `PipeReader` and `PipeWriter` can be used to efficiently
  read and write to `Stream`, including backpressure and flow control,
  with minimal allocations.
 - If arrays have to be created, best is to use `MemoryPool<T>.Shared.
   Rent(size)` and `Dispose()` it when done. Note that you can create a
   `Span<T>` from `T[]`.
 - `Encoding.UTF8.GetBytes(ReadOnlySpan<char>,Span<byte>)` doesn't
   allocate and uses only the `Span<byte>`. The same applies to
   `Encoding.UTF8.GetChars(ReadOnlySpan<byte>,Span<char>)`. To get the
   size of the `Span<byte>` and `Span<char>` right, use `Encoding.UTF8.
   GetByteCount(ReadOnlySpan<char>)` and `Encoding.UTF8.
   GetCharCount(ReadOnlySpan<byte>)` respectively .
- `Encoding.UTF8.GetDecoder()` must be used in combination with
  `PipeReader`, because the underlying `Stream` may not be complete,
  but the last `byte` doesn't always have to match with an UTF-8
  character. The `Decoder` keeps those remaining `bytes` for the next
  call. The workings are for the identical to `Encoding.UTF8`.
- A `ReadOnlySequence<T>` can only be created by creating a new `class`
  that derives from `ReadOnlySequenceSegment<T>`. Because this base
  `class` has some weight, expect at least `48 bytes` to be used to
  hold the reference (and more if you add fields). However it doesn't
  matter how big the `ReadOnlyMemory<T>` block is you put in it: the
  cost is constant.
- `IAsyncEnumerable<T>` can be used to create 'streams' of other types
  than `byte`. It is similar to a regular `yield return`, but now
  asynchronous. However, with this approach, at most one element wil be
  on the 'stream': the producer can't create more elements when the
  consumer is paused or slower in specific scenarios.
- `Channel` can be used to create 'streams' of other types than `byte`.
  It has familiar features compared to `Pipe`: it also has a reader and
  writer as `ChannelReader<T>` and `ChannelWriter<T>` respectively. The
  major difference is that `PipeReader` is able to examine a part of
  the buffer, but not consume it, giving it a sort of look-ahead
  functionality. The benefit of `Channel` is that it allows for both
  multiple readers and writers at the same time.
- UTF-8 is the default for any HTML and it's one of the easiest to
  process, therefore limiting the implementation to only UTF-8 should
  not hold any problems. It is important to note that &ndash; although
  not recommended &ndash; a *byte order mark* (BOM) may be present as
  the **very** start. It holds no value, because no matter the
  endianness of the system, the order of multi-byte UTF-8 characters
  are always the same.

  | Version | Endianness | Hex              | Binary                                          |
  | ------- | ---------- | ---------------- | ----------------------------------------------- |
  | UTF-8   | -          | `EF BB BF`       | `1110 1111` `1011 1011` `1011 1111‬`             |
  | UTF-16  | Big        | `FE FF`          | `1111 1110` `1111 1111`                         |
  | UTF-16  | Little     | `FF FE`          | `1111 1111` `1111 1110`                         |
  | UTF-32  | Big        | `00 00 FE FF`    | `0000 0000` `0000 0000` `1111 1110` `1111 1111` |
  | UTF-32  | Little     | `FF FE 00 00`    | `1111 1111` `1111 1110` `0000 0000` `0000 0000` |
  | UTF -7  | -          | `2B 2F 76 38`<br>`2B 2F 76 39`<br>`2B 2F 76 2B`<br>`2B 2F 76 2F`<br>`2B 2F 76 38 2D` | `001 1011` `0010 1111` `0111 0110` `0011 1000`<br>`001 1011` `0010 1111` `0111 0110` `0011 1001`<br>`001 1011` `0010 1111` `0111 0110` `0010 1011`<br>`001 1011` `0010 1111` `0111 0110` `0010 1111`<br>`001 1011` `0010 1111` `0111 0110` `0011 1000` `0010 1101`

- The characters denoting HTML elements are `<` and `>`. Because  these
  all reside in the ASCII range, they are encoded with exactly 1
  `byte`. It's therefore possible to search for them directly in a
  buffer. The same holds true for carriage return (`CR` or `\r`) and
  line feed (*newline*, *line ending*, *end of line*, *line break*,
  `LF` or `EOL`). Note however, often `CRLF` (`\r\n`) is used which
  takes **two** `bytes`. They may span over two segments as well.

  | Char | Hex  | Binary      |
  | ---- | ---- | ----------- |
  | `<`  | `3C` | `0011 1100` |
  | `>`  | `3E` | `0011 1110` |
  | `CR` | `0D` | `0000 1101` |
  | `LF` | `0A` | `0000 1010` |
- In general in HTML, multiple spaces (*linear whitespace*,
  *whitespace* or `SP`) are reduced to one. There are exceptions, such
  as (but not limited to) `<pre>` and attributes. Browsers tend to
  treat newlines (no matter if `CR`, `LF` or `CRLF`) as a space. This
  makes a newline followed by some spaces for indentation to become one
  space. Same holds true for multiple successive newlines.
- The HTML spec is available at
  [html.whatwg.org](https://html.spec.whatwg.org/multipage/). It also
  has information about parsing in
  [chapter 12](https://html.spec.whatwg.org/multipage/parsing.html). 
- Tags can be found by searching for `<` and `>`, but note the
  following. This means a naive search for only `<` and `>` is not
  correct to properly detect tags. A `>` can in general be placed
  almost anywhere and mostly only gains value in context because of a
  `<`. The same holds true for `<`, albeit it requiring no directly
  following `a-Z` to function as text or comment (`<!` and `<?` are
  explicit exceptions).
  - **Errors and warnings**
    - Various `DOCTYPE` errors regarding early `>`.
    - `invalid-first-character-of-tag-name` &rArr; `<` followed by
      something else than `a-Z/` (e.g. `<42>`) is considered text instead
      of a tag. If there's no matching open tag, it is considered a
      comment instead of text (e.g. `</42>`)`.
    - `missing-end-tag-name` &rArr; when an end tag name is expected, but
      a `>` was found is ignored completely (e.g. `</>`)
    - `unexpected-character-in-attribute-name` &rArr; an attribute
      name contains a `<` (e.g. `<div foo<div>` becomes an attribute
      `foo<div`). This doesn't apply to `>`.
    - `unexpected-character-in-unquated-attribute-value` &rArr; an
      unquoted attribute value contains a `<` (e.g. `div foo=b<ar>`
      becomes an attribute value `b<ar`). This doesn't apply to `>`.
  - **Tag structure**
    - *Start tag* &rArr; `<` + *tag name* + ` ` + *attributes* + ` *`
      + (`/`) + `>`. Only *void elements* and *foreign elements* may
      have the closing `/` on the start tag.
    - *End tag* &rArr; `<` + `/` + *tag name* + ` *` + `>`
    - *Tag name* &rArr; `a-Z` + `a-Z0-9*`
    - *Tag attribute* &rArr; *attribute name* + ` `* (`=` ` *` (`'"`) +
      *attribute value* + (`'"`)). A value is optional (first set of
      parentheses).
      - Empty &arr; `<input disabled>`
      - Unquoted &rarr; `<input value=yes>`
      - Single quoted &rarr; `<input type='checkbox'>`
      - Double quoted &rarr; `<input name="be evil">`
    - *Attribute name* &rArr; while technically almost any character is
      allowed other than special characters and `'">/=`, it is
      convention to use only `a-Z` + `a-Z09*`
    - *Attribute value* &rArr; technically almost any character, except
      those that would break the format. This means while being a
      single quoted attribute, the value can't contain `'` as it would
      close the value. In those scenarios character references must be
      used (e.g. `&apos;` for `'`).
  - **Foreign tags**\
    These reside in either the *MathML* namespace
    (`http://www.w3.org/1998/Math/MathML`) or *SVG* namespace
    (`http://www.w3.org/2000/svg`). These tags also allow namespaces
    for their attributes (e.g. `xlink:actuate` and `xml:space`). This
    is a predefined list found at
    [chapter 12.1.2.3](https://html.spec.whatwg.org/multipage/syntax.html#attributes-2).
